library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all; 
use ieee.std_logic_unsigned.all;

entity automate is
    port(
      clk_i, reset_i : in std_logic;
      door_i, button_stop, button_wave : in std_logic;
      -- Il nous manque la sortie du circuit qui active ou pas les ondes: wave_o par exemple
      wave_o : out std_logic;
      light_o : out std_logic
   );    
    end automate;

architecture arch_automate of automate is
  -- Les états doivent correspondre à ceux du schéma: fonctionne et ne_fonctionne_pas (dans un premier temps)
  type run_state_t is (running, stopped );
  signal run_state_current, run_state_next : run_state_t;
  -- Il faut utiliser des nom très explicit qui nous permettent de comprendre à quoi sert le signal
  signal counter_10_end : std_logic;
  signal counter_10 : unsigned(27 downto 0);
  --signal wave_sig : std_logic;
  
  type light_state_t is (light_on, light_off, extinction );
  signal light_state_current, light_state_next : light_state_t;
  signal counter_15_end : std_logic;
  signal counter_15 : unsigned(28 downto 0);

  --type state_t is (alarm_on, alarm_off );
  --signal state_reg, state_next : state_t;
  --signal alarm_sig : std_logic;

  --type state_t is (block_door_work, block_door_doesn't_work );
  --signal state_reg, state_next : state_t;
  --signal block_door_sig : std_logic;


  begin
  
  proc_counter_10 : process (clk_i) begin
    if reset_i = '0' then
      counter_10 <= (others => '0');
    elsif rising_edge(clk_i) then
      if counter_10 = 250000000 or run_state_current = stopped  then
        counter_10 <= (others => '0');
      elsif run_state_current = running then
        counter_10 <= counter_10 + 1;
      else
        counter_10 <= counter_10;
      end if;
    end if;
  end process;

  counter_10_end <= '1' when counter_10 = 250000000 else '0';

  proc_logic_gates_run: process(all)
  begin
    run_state_next <= run_state_current;
    
    case run_state_current is
      when running =>
        if button_stop = '0' then
          run_state_next <= stopped;
        elsif counter_10_end = '1' then
          run_state_next <= stopped;
        end if;

      when stopped =>
          if button_wave = '0' then
            run_state_next <= running;
          end if;
    end case;
  end process;

  mem_run : process (clk_i)
  begin
    -- Bien mais cette mémorisation doit se faire à chaque période (indépendament de alpha, beta... etc)
    if reset_i = '0' then
      run_state_current <= stopped;
    elsif rising_edge(clk_i) then
      run_state_current <= run_state_next;
    end if;
  end process;
  
  wave_o <= '1' when run_state_current = stopped else '0';
  
    --############################################################
    --############################################################
    --############################################################
    
  proc_counter_15: process (clk_i) begin
    if reset_i = '0' then
      counter_15 <= (others => '0');
    elsif rising_edge(clk_i) then
      if counter_15 = 375000000 or light_state_current /= extinction  then
        counter_15 <= (others => '0');
      elsif light_state_current = extinction then
        counter_15 <= counter_15 + 1;
      else
        counter_15 <= counter_15;
      end if;
    end if;
  end process;

  counter_15_end <= '1' when counter_15 = 375000000 else '0';

  proc_logic_gates_light: process(all)
  begin
    light_state_next <= light_state_current;
    
    case light_state_current is
      when light_off =>
        if door_i = '0' then
          light_state_next <= light_on;
        elsif run_state_current = running then
          light_state_next <= light_on;
        end if;

      when light_on =>
        if door_i = '1' then
          light_state_next <= extinction;
        elsif run_state_current = stopped then
          light_state_next <= extinction;
        end if;
      when extinction =>
        if counter_15_end = '1' then 
          light_state_next <= light_off;
        end if;
    end case;
  end process;

  mem_light : process(clk_i)
  begin
    if reset_i = '0' then
      light_state_current <= light_off;
    elsif rising_edge(clk_i) then
      light_state_current <= light_state_next;
    end if;
  end process;
  
  light_o <= '1' when light_state_current = light_off else '0';

  end architecture arch_automate;