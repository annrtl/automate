# Package de base de Python
import os

# Package de NanoXplore
from nxpython import *

# Chemins des dossier
project_dir = os.path.dirname(os.path.abspath(__file__))
source_dir = os.path.join(project_dir, 'src')
output_dir = os.path.join(project_dir, 'output')
logs_dir = os.path.join(project_dir, 'logs')

# Création du projet Impulse
project = createProject(output_dir)

# Definition du dossier de logs
setLogDirectory(logs_dir)

# Choix du FPGA
project.setVariantName('NG-MEDIUM')

# Choix de l'entité à programmer
project.setTopCellName('automate')

# Ajout des fichiers souces (circuit)
project.addFiles([
    os.path.join (source_dir, 'automate.vhd'),
])

"""
project.setOptions({
    'ManageAsynchronousReadPort': 'Yes', 
    'ManageUnconnectedOutputs': 'Ground', 
    'ManageUnconnectedSignals': 'Ground',
    'DefaultRAMMapping': 'RAM', 
    'MaxRegisterCount': '12500',
})

project.setOptions({
    'MappingEffort': 'High', 
    'TimingDriven': 'Yes'
})

project.setOptions({
    'MappingEffort': 'High', 
    'TimingDriven': 'Yes', 
    'DensityEffort': 'Medium', 
    'BypassingEffort': 'High', 
    'PartitioningEffort': 'High', 
    'PolishingEffort': 'High', 
    'RoutingEffort': 'High'
})
"""

# Connecter les entrées/sorties aux pads du FPGA
# https://nanoxplore-wiki.atlassian.net/wiki/spaces/NAN/pages/34471937/DevKit+V3+User+Guide
project.addPad('clk_i'          , {'location': 'IOB12_D09P'})
project.addPad('reset_i'        , {'location': 'IOB10_D12N'})
project.addPad('door_i'    ,      {'location': 'IOB10_D09P'})
project.addPad('button_stop'    , {'location': 'IOB10_D12P'})
project.addPad('button_wave'    , {'location': 'IOB10_D07N'})
project.addPad('button_light'   , {'location': 'IOB10_D14P'})
project.addPad('wave_o'         , {'location': 'IOB0_D01P'})
project.addPad('light_o'        , {'location': 'IOB0_D03N'})

#project.addPad('K' , {'location': 'IOB0_D03N'})
#project.addPad('L' , {'location': 'IOB0_D03P'})

# Lancer les trois étapes de compilations
project.synthesize()
project.save("automate_synth.vhd")
project.place()
project.save("automate_place.vhd")
project.route()
project.save("automate_route.vhd")

# Obtenir le fichier binaire
project.generateBitstream(os.path.join(project_dir, 'project.nxb'))
#project.nxpython(os.path.join(project_dir, 'project.nxb'))